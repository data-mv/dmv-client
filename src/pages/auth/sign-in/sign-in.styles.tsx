import styled from 'styled-components';
// @ts-ignore
import { palette } from 'styled-theme';
import WithDirection from '../../../settings/withDirection';

interface ISignInStyleProps {
  "data-rtl": string
}

const SignInStyleWrapper = styled.div<ISignInStyleProps>`
  .isoSignInForm {
    width: 100%;
    display: flex;
    flex-shrink: 0;
    flex-direction: column;

    .isoInputWrapper {
      input {
        &::-webkit-input-placeholder {
          color: ${palette('grayscale', 0)};
        }

        &:-moz-placeholder {
          color: ${palette('grayscale', 0)};
        }

        &::-moz-placeholder {
          color: ${palette('grayscale', 0)};
        }
        &:-ms-input-placeholder {
          color: ${palette('grayscale', 0)};
        }
      }
      
      .ant-form-item-explain {
        font-size: 11px;
      }
      &.isoCheckbox {
        margin-bottom: 10px;
      }
    }

    .isoHelperWrapper {
      margin-top: 10px;
      flex-direction: column;
    }

    .isoForgotPass {
      font-size: 12px;
      color: ${palette('text', 3)};
      margin-bottom: 10px;
      text-decoration: none;

      &:hover {
        color: ${palette('primary', 0)};
      }
    }

    button {
      font-weight: 500;
      width: 100%
    }
  }
`;

export default WithDirection(SignInStyleWrapper);
