import React, {RefObject} from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Form } from 'antd';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import SignInStyleWrapper from './sign-in.styles';
import StyledInput, { StyledPassword } from '../../../components/input/input.component';
import Checkbox from '../../../components/checkbox/checkbox.component';
import IntlMessages from '../../../components/utility/intlMessages';
import Button from '../../../components/button/button.component';
import AuthBackground from '../../../components/auth-background/auth-background.component';
import {errorNotification, successNotification} from "../../../components/utility/notifications";
import {createStructuredSelector, Selector} from "reselect";
import {selectAuth, selectAuthStart} from "../../../redux/auth/auth.selectors";
import { resetAuthState } from "../../../redux/auth/auth.actions";
import { loginOperation } from "../../../redux/auth/auth.thunk";
import {FormInstance, Rule} from "antd/lib/form";
import {IState} from "../../../interface";
import {IAuthState} from "../../../redux/auth/auth.interface";
import {AnyAction, Dispatch} from "redux";
import { CheckboxChangeEvent } from "antd/lib/checkbox";
import bgImage from '../../../image/janko.jpg';
import RouteObject from "../../../routes";


interface ISignInState {
  rememberMe: boolean
}
type ISignInProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>
interface ISignInFormValues {
  email: string,
  password: string
}
type IMapStateToProps = Selector<IState, {
  auth: IAuthState,
  isStarting: boolean
}>;
interface IMapDispatchToProps {
  (dispatch: Dispatch): {
    resetAuthStateOperation: () => AnyAction,
    loginOperation: (payload: ISignInFormValues) => AnyAction
  }
}

class SignIn extends React.Component<ISignInProps, ISignInState> {
  private readonly usernameRules: Rule[];
  private readonly passwordRules: Rule[];
  private readonly form: RefObject<FormInstance>;

  constructor(props: ISignInProps) {
    super(props);
    this.usernameRules = [
      {
        required: true,
        message: "Please input your email!",
        type: "email"
      }
    ];
    this.passwordRules = [
      {
        required: true,
        message: 'Please input your password!',
      }
    ];
    this.state = {
      rememberMe: false,
    };
    this.form = React.createRef();
  }

  private onFinish = (values: ISignInFormValues) => {
    const { rememberMe } = this.state;
    if (rememberMe) {
      localStorage.setItem("rememberMe", rememberMe.toString());
      localStorage.setItem("email", values.email);
    } else {
      localStorage.removeItem("rememberMe");
      localStorage.removeItem("email");
    }
    this.props.loginOperation(values);
  };

  private onChange = (event: CheckboxChangeEvent) => {
    this.setState({rememberMe: event.target.checked});
  };

  public componentDidMount() {
    const rememberMe: string | null = localStorage.getItem("rememberMe");
    const email: string | null = localStorage.getItem("email");
    if (rememberMe && email) {
      this.setState({
        rememberMe: true,
      });
      this.form.current && this.form.current.setFieldsValue({email});
    }
    if(this.props.auth.success) {
      const { message, description} = this.props.auth;
      successNotification({
        message,
        description,
        onClose: this.props.resetAuthStateOperation
      })
    }
  }

  public componentDidUpdate(prevProps: Readonly<ISignInProps>, prevState: Readonly<{}>, snapshot: any) {
    const { success, error } = this.props.auth;
    if (!success && error) {
      const { message, formatted_error } = error;
      errorNotification({
        message: message,
        description: formatted_error,
        onClose: this.props.resetAuthStateOperation
      });
    }
  }

  render() {
    return (
      <AuthBackground bgImage={bgImage}>
        <SignInStyleWrapper>
          <Form ref={this.form} onFinish={this.onFinish} name="loginForm">
            <div className="isoSignInForm">
              <div className="isoInputWrapper">
                <Form.Item
                  rules={this.usernameRules}
                  name="email"
                  hasFeedback
                >
                  <StyledInput
                    type="email"
                    size="large"
                    placeholder="Email"
                  />
                </Form.Item>
              </div>

              <div className="isoInputWrapper">
                <Form.Item rules={this.passwordRules} name="password" hasFeedback>
                  <StyledPassword
                    placeholder="Password"
                    iconRender={(visible: boolean) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                  />
                </Form.Item>
              </div>
              <div className="isoInputWrapper isoCheckbox">
                <Checkbox onChange={this.onChange} checked={this.state.rememberMe}>
                  <IntlMessages id="page.signInRememberMe" />
                </Checkbox>
              </div>
              <div className="isoInputWrapper">
                <Form.Item>
                  <Button type="primary"  htmlType="submit">
                    <IntlMessages
                      id={
                        this.props.isStarting
                          ? "page.authenticatingButton"
                          : "page.signInButton"
                      }
                    />
                  </Button>
                </Form.Item>
              </div>
              <div className="isoCenterComponent isoHelperWrapper">
                <Link to={RouteObject.passwordReset} className="isoForgotPass">
                  <IntlMessages id="page.signInForgotPassword" />
                </Link>
                <Link to={RouteObject.signUp}>
                  <IntlMessages id="page.signInCreateAccount" />
                </Link>
              </div>
            </div>
          </Form>
        </SignInStyleWrapper>
      </AuthBackground>
    );
  }
}
const mapStateToProps: IMapStateToProps = createStructuredSelector({
  isStarting: selectAuthStart,
  auth: selectAuth
});

const mapDispatchToProps: IMapDispatchToProps = (dispatch) => ({
  resetAuthStateOperation: () => dispatch(resetAuthState()),
  loginOperation: (payload) => dispatch(loginOperation(payload) as unknown as AnyAction)
});

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);