import styled from 'styled-components';
// @ts-ignore
import { palette } from 'styled-theme';
import WithDirection from '../../../settings/withDirection';


const PasswordResetStyleWrapper = styled.div`
  .isoFormHeadText {
    width: 100%;
    display: flex;
    flex-direction: column;
    margin-bottom: 15px;
    justify-content: center;

    h3 {
      font-size: 13px;
      font-weight: 600;
      line-height: 1.2;
      margin: 0 0 7px;
      color: ${palette('text', 0)};
    }

    p {
      font-size: 13px;
      font-weight: 400;
      line-height: 1.2;
      margin: 0;
      color: ${palette('text', 2)};
    }
  }
  .isoPasswordResetForm {
    width: 100%;
    display: flex;
    flex-shrink: 0;
    flex-direction: column;

    .isoInputWrapper {
      margin-bottom: 10px;

     .ant-form-item {
       margin-bottom: 0;
     }

      &:last-child {
        margin-bottom: 0;
      }

      input {
        &::-webkit-input-placeholder {
          color: ${palette('grayscale', 0)};
        }

        &:-moz-placeholder {
          color: ${palette('grayscale', 0)};
        }

        &::-moz-placeholder {
          color: ${palette('grayscale', 0)};
        }
        &:-ms-input-placeholder {
          color: ${palette('grayscale', 0)};
        }
      }
      .ant-form-item-explain {
        font-size: 11px;
      }

      button {
        height: 42px;
        width: 100%;
        font-weight: 500;
        font-size: 13px;
      }
    }
  }

`;

export default WithDirection(PasswordResetStyleWrapper);