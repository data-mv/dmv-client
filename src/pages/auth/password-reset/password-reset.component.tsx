import React from 'react';
import { Form } from 'antd';
import AuthBackground from "../../../components/auth-background/auth-background.component";
import Button from "../../../components/button/button.component";
import IntlMessages from '../../../components/utility/intlMessages';
import bgImage from '../../../image/image3.jpg';
import StyledInput from "../../../components/input/input.component";
import { Rule } from 'antd/lib/form';
import PasswordResetStyleWrapper from './password-reset.styles';
import {connect} from "react-redux";
import {createStructuredSelector} from "reselect";
import {selectPasswordResetState} from "../../../redux/auth/auth.selectors";
import {passwordResetOperation} from "../../../redux/auth/auth.thunk";
import {IState} from "../../../interface";
import {IAuthState} from "../../../redux/auth/auth.interface";
import {AnyAction, Dispatch} from "redux";
import {errorNotification, successNotification} from "../../../components/utility/notifications";
import {resetAuthState} from "../../../redux/auth/auth.actions";
import {Selector} from "reselect/src";

type IPasswordResetProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>
type IMapStateToProps = Selector<IState, {
  data: Pick<IAuthState, "success" | "error" | "data">
}>;
interface IMapDispatchToProps {
  (dispatch: Dispatch): {
    passwordResetOperation: (payload: {email: string}) => AnyAction,
    resetAuthStateOperation: () => AnyAction
  }
}

class PasswordReset extends React.Component<IPasswordResetProps, {}> {
  private readonly rules: Rule[];
  
  constructor(props: IPasswordResetProps) {
    super(props);
    this.rules = [
      {
        required: true,
        message: "Please input a valid email",
        type: "email"
      }
    ]
  }

  private onFinish = (values: {email: string}) => {
    this.props.passwordResetOperation(values);
  };

  public componentDidUpdate(prevProps: Readonly<IPasswordResetProps>, prevState: Readonly<{}>, snapshot: any) {
    const { success, error, data } = this.props.data;
    if (!success && error) {
      const { message, formatted_error } = error;
      errorNotification({
        message: message,
        description: formatted_error,
        onClose: this.props.resetAuthStateOperation
      });
    }

    if (success && data) {
      const { message } = data;
      successNotification({
        message: "Reset Link Sent",
        description: message,
        onClose: this.props.resetAuthStateOperation
      });
    }
  }


  render() {
    return (
      <AuthBackground bgImage={bgImage}>
        <PasswordResetStyleWrapper>
          <div className="isoFormHeadText">
            <h3>
              <IntlMessages id="page.passwordResetSubtitle" />
            </h3>
            <p>
              <IntlMessages id="page.passwordResetDescription" />
            </p>
          </div>
          <Form onFinish={this.onFinish}>
            <div className="isoPasswordResetForm">
              <div className="isoInputWrapper">
                <Form.Item
                  rules={this.rules}
                  name="email"
                  hasFeedback
                >
                  <StyledInput
                    type="email"
                    size="large"
                    placeholder="Email"
                  />
                </Form.Item>
              </div>
              <div className="isoInputWrapper">
                <Form.Item>
                  <Button type="primary" htmlType="submit">
                    <IntlMessages id="page.passwordResetButton" />
                  </Button>
                </Form.Item>
              </div>
            </div>
          </Form>
        </PasswordResetStyleWrapper>
      </AuthBackground>
    );
  }
}

const mapStateToProps: IMapStateToProps = createStructuredSelector({
  data: selectPasswordResetState
});

const mapDispatchToProps: IMapDispatchToProps = (dispatch) => ({
  passwordResetOperation: (payload) => dispatch(passwordResetOperation(payload) as unknown as AnyAction),
  resetAuthStateOperation: () => dispatch(resetAuthState())
});

export default connect(mapStateToProps, mapDispatchToProps)(PasswordReset);