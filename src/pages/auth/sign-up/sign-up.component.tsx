import React from 'react';
import { connect } from 'react-redux';
import {Link, Redirect} from 'react-router-dom';
import { Form } from 'antd';
import {createStructuredSelector, Selector} from "reselect";
import IntlMessages from '../../../components/utility/intlMessages';
import StyledInput from "../../../components/input/input.component";
import Checkbox from '../../../components/checkbox/checkbox.component';
import Button from '../../../components/button/button.component';
import AuthBackground from '../../../components/auth-background/auth-background.component';
import { errorNotification } from "../../../components/utility/notifications";
import {signUpOperation} from "../../../redux/auth/auth.thunk";
import { selectAuth, selectAuthStart } from "../../../redux/auth/auth.selectors";
import { resetAuthState } from "../../../redux/auth/auth.actions";
import {AnyAction, Dispatch} from "redux";
import {IState} from "../../../interface";
import {IAuthState} from "../../../redux/auth/auth.interface";
import SignUpStyleWrapper from "./sign-up.styles";
import {Rule} from "antd/lib/form";
import { CheckboxChangeEvent } from "antd/lib/checkbox";
import bgImage from '../../../image/data-box.jpg';
import RouteObject from "../../../routes";
import PasswordForm from "../../../components/password-form/password-form.component";


interface ISignUpState {checked: boolean}
type ISignUpProps = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>
interface ISignUpFormValues {
  email: string,
  termsAndConditions: boolean
}
type IMapStateToProps = Selector<IState, {
  auth: IAuthState,
  isStarting: boolean
}>;
interface IMapDispatchToProps {
  (dispatch: Dispatch): {
    signUpOperation: (data: ISignUpFormValues) => AnyAction,
    resetAuthStateOperation: () => AnyAction
  }
}

class SignUp extends React.Component<ISignUpProps, ISignUpState> {
  private readonly rules: Rule[];
  constructor(props: ISignUpProps) {
    super(props);
    this.state = {
      checked: true
    };
    this.rules = [
      {
        required: true,
        message: "Please input a valid email",
        type: "email"
      }
    ];
  }

  private onFinish = (values: ISignUpFormValues): void => {
    this.props.signUpOperation(values);
  };

  private onCheck = (event: CheckboxChangeEvent):void => {
    this.setState({
      checked: event.target.checked
    })
  };

  public componentDidUpdate(prevProps: Readonly<ISignUpProps>, prevState: Readonly<ISignUpState>, snapshot: any): void {
    const { success, error } = this.props.auth;
    if ((prevState.checked === this.state.checked) && !success && error) {
      const { message, formatted_error } = error;
      errorNotification({
        message: message,
        description: formatted_error,
        onClose: this.props.resetAuthStateOperation
      });
    }
  }

  render() {
    const { success } = this.props.auth;
    if (success) {
      return <Redirect to={RouteObject.login}/>
    }
    return (
      <AuthBackground bgImage={bgImage}>
        <SignUpStyleWrapper>
          <Form onFinish={this.onFinish} >
            <div className="isoSignUpForm">
              <div className="isoInputWrapper">
                <Form.Item
                  rules={this.rules}
                  name="email"
                  hasFeedback
                >
                  <StyledInput
                    type="email"
                    size="large"
                    placeholder="Email"
                  />
                </Form.Item>
              </div>
              <PasswordForm />
              <div className="isoInputWrapper" style={{ marginBottom: "30px" }}>
                <Form.Item name="termsAndConditions">
                  <Checkbox checked={this.state.checked} onChange={this.onCheck}>
                    <IntlMessages id="page.signUpTermsConditions" />
                  </Checkbox>
                </Form.Item>
              </div>
              <div className="isoInputWrapper">
                <Form.Item>
                  <Button type="primary" htmlType="submit" disabled={!this.state.checked}>
                    <IntlMessages id={ this.props.isStarting ? "page.signUpStart" : "page.signUpButton" } />
                  </Button>
                </Form.Item>
              </div>
            </div>
          </Form>
          <div className="isoInputWrapper isoCenterComponent isoHelperWrapper">
            <Link to={RouteObject.login}>
              <IntlMessages id="page.signUpAlreadyAccount" />
            </Link>
          </div>
        </SignUpStyleWrapper>
      </AuthBackground>
    )
  }
}

const mapStateToProps: IMapStateToProps = createStructuredSelector({
  isStarting: selectAuthStart,
  auth: selectAuth
});

const mapDispatchToProps: IMapDispatchToProps = (dispatch) => ({
  signUpOperation: (data) => dispatch(signUpOperation(data) as unknown as AnyAction),
  resetAuthStateOperation: () => dispatch(resetAuthState())
});

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);