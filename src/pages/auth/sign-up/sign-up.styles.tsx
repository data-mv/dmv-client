import styled from 'styled-components';
// @ts-ignore
import { palette } from 'styled-theme';
import WithDirection from '../../../settings/withDirection';

interface ISignUpStyleProps {
  "data-rtl": string,
  className?: string
}


const SignUpStyleWrapper = styled.div<ISignUpStyleProps>`
  .isoSignUpForm {
    width: 100%;
    flex-shrink: 0;
    display: flex;
    flex-direction: column;

    .isoInputWrapper {
      margin-bottom: 15px;
      
      div.ant-row {
        margin-bottom: 5px;
      }

      &:last-child {
        margin-bottom: 0;
      }
      
      .ant-input {
        font-size: 13px;
      }

      input {
        &::-webkit-input-placeholder {
          color: ${palette('grayscale', 0)};
        }

        &:-moz-placeholder {
          color: ${palette('grayscale', 0)};
        }

        &::-moz-placeholder {
          color: ${palette('grayscale', 0)};
        }
        &:-ms-input-placeholder {
          color: ${palette('grayscale', 0)};
        }
      }
    }
    
    button {
      font-weight: 500;
      width: 100%;
      height: 42px;
      border: 0;
    }
  }
`;

export default WithDirection(SignUpStyleWrapper);
