import React, {useEffect} from 'react';
import {createStructuredSelector, Selector} from "reselect";
import {connect} from "react-redux";
import {Result, Button} from 'antd';
import RouteObject from "../../routes";
import WithErrorPages, {IWithErrorPages} from "../../components/hoc/WithErrorPages";
import {AnyAction, compose, Dispatch} from "redux";
import {RouteComponentProps} from 'react-router';
import ContainerSpin from "../../components/spin/spin.component";
import {verifyApiKeyOperation} from "../../redux/auth/auth.thunk";
import {resetAuthState} from "../../redux/auth/auth.actions";
import {IState} from "../../interface";
import {IAuthState} from "../../redux/auth/auth.interface";
import {selectActivatedAccountState} from "../../redux/auth/auth.selectors";


type IProps = ReturnType<typeof mapStateToProps>
  & ReturnType<typeof mapDispatchToProps>
  & RouteComponentProps<any>
  & IWithErrorPages;

type IMapStateToProps = Selector<IState, {
  data: Pick<IAuthState, "success" | "error" | "activated">
}>;

interface IMapDispatchToProps {
  (dispatch: Dispatch): {
    resetAuthStateOperation: () => AnyAction;
    activateAccount: (payload: {apiKey: string, path: string}) => AnyAction;
  }
}

const AccountValidationPage: React.FunctionComponent<IProps> = (props) => {
  const {path, params: { key }} = props.match;
  const { activated, error } = props.data;
  const { serverError, forbiddenError }= props;
  useEffect(() => {
    props.activateAccount({apiKey: key, path});
    return function resetAuthState() {
      props.resetAuthStateOperation();
    }
  },[]);

  if (!activated && !error) {
    return <ContainerSpin tip="Verifying Link..."/>
  }

  if (!activated && error) {
    switch (error.status_code) {
      case 500:
        return serverError(error.formatted_error[0]);
      case 403:
        return forbiddenError(error.formatted_error[0]);
    }
  }


  return <Result
    status="success"
    title="Account Validated"
    subTitle="Your account has been successfully validated"
    extra={
      <Button type="primary" href={RouteObject.login}>
        Go To Login
      </Button>
    }
  />;
};

const mapStateToProps: IMapStateToProps = createStructuredSelector({
  data: selectActivatedAccountState
});

const mapDispatchToProps: IMapDispatchToProps = (dispatch) => ({
  activateAccount: (payload) => dispatch(verifyApiKeyOperation(payload) as unknown as AnyAction),
  resetAuthStateOperation: () => dispatch(resetAuthState())
});

export default compose(
  WithErrorPages,
  connect(mapStateToProps, mapDispatchToProps)
)(AccountValidationPage);
