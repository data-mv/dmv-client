import React from 'react';
import AuthBackground from '../../../components/auth-background/auth-background.component';
import bgImage from '../../../image/image5.jpg';
import IntlMessages from "../../../components/utility/intlMessages";
import {Form} from "antd";
import Button from "../../../components/button/button.component";
import UpdatePasswordStyleWrapper from "./update-password.styles";
import {passwordUpdateOperation, verifyApiKeyOperation} from "../../../redux/auth/auth.thunk";
import {createStructuredSelector} from "reselect";
import {selectUpdatePasswordState} from "../../../redux/auth/auth.selectors";
import {connect} from "react-redux";
import {AnyAction, compose, Dispatch} from "redux";
import {IState} from "../../../interface";
import {IAuthState} from "../../../redux/auth/auth.interface";
import {Selector} from "reselect/src";
import {RouteComponentProps} from "react-router";
import ContainerSpin from "../../../components/spin/spin.component";
import {resetAuthState} from "../../../redux/auth/auth.actions";
import RouteObject from "../../../routes";
import {Redirect} from "react-router-dom";
import WithErrorPages, {IWithErrorPages} from "../../../components/hoc/WithErrorPages";
import PasswordForm from "../../../components/password-form/password-form.component";

type IUpdatePasswordProps = ReturnType<typeof mapStateToProps>
  & ReturnType<typeof mapDispatchToProps>
  & RouteComponentProps<any>
  & IWithErrorPages;

type IMapStateToProps = Selector<IState, {
  data: Pick<IAuthState, "success" | "error" | "data" | "verified">
}>;
interface IMapDispatchToProps {
  (dispatch: Dispatch): {
    verifyApiKey: (payload: {apiKey: string, path: string}) => AnyAction,
    updatePasswordOperation: (payload: {apiKey: string, password: string}) => AnyAction,
    resetAuthStateOperation: () => AnyAction
  }
}


class UpdatePasswordComponent extends React.Component<IUpdatePasswordProps, {}> {

  private onFinish = (values: {password: string, confirm: string}) => {
    this.props.updatePasswordOperation({
      apiKey: this.props.match.params.key,
      password: values.password
    })
  };

  public componentDidMount() {
    this.props.resetAuthStateOperation();
    const { params: {key}, path } = this.props.match;
    this.props.verifyApiKey({apiKey: key, path});
  }


  render() {
    const { success, error, verified } = this.props.data;
    const { serverError, forbiddenError } = this.props;
    if (!verified && !error) {
      return <ContainerSpin tip="Verifying Link..."/>
    }
    if (!verified && error) {
      switch (error.status_code) {
        case 500:
          return serverError(error.formatted_error);
        case 403:
          return forbiddenError(error.formatted_error);
      }
    }
    if (success) {
      return <Redirect to={RouteObject.login}/>
    }
    return (
      <AuthBackground bgImage={bgImage}>
        <UpdatePasswordStyleWrapper>
          <div className="isoFormHeadText">
            <h3>
              <IntlMessages id="page.passwordUpdateSubtitle" />
            </h3>
            <p>
              <IntlMessages id="page.passwordUpdateDescription" />
            </p>
          </div>
          <Form onFinish={this.onFinish}>
            <div className="isoPasswordUpdateForm">
              <PasswordForm />
              <div className="isoInputWrapper">
                <Form.Item>
                  <Button type="primary" htmlType="submit">
                    <IntlMessages id="page.passwordUpdateButton" />
                  </Button>
                </Form.Item>
              </div>
            </div>
          </Form>
        </UpdatePasswordStyleWrapper>
      </AuthBackground>
    )
  }
}

const mapDispatchToProps: IMapDispatchToProps = (dispatch) => ({
  verifyApiKey: (payload) => dispatch(verifyApiKeyOperation(payload) as unknown as AnyAction),
  updatePasswordOperation: (payload) => dispatch(passwordUpdateOperation(payload) as unknown as AnyAction),
  resetAuthStateOperation: () => dispatch(resetAuthState())
});

const mapStateToProps: IMapStateToProps = createStructuredSelector({
  data: selectUpdatePasswordState
});

export default compose(
  WithErrorPages,
  connect(mapStateToProps, mapDispatchToProps)
)(UpdatePasswordComponent);
