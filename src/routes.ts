const RouteObject = {
  login: '/login',
  signUp: '/signup',
  passwordReset: '/forgot-password',
  homepage: '/',
  dashboard: '/dashboard',
  updatePassword: '/reset-password/:key',
  validateAccount: '/validate/:key',
};

export default RouteObject;

