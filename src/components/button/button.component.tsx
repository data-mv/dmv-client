import { Button } from 'antd';
import { ButtonStyle } from './button.styles';
import WithDirection from '../../settings/withDirection';

const AntButton = ButtonStyle(Button);
const isoButton = WithDirection(AntButton);

export default isoButton;