import React from 'react';
import {Button as AntButton, Result} from "antd";
import RouteObject from "../../routes";

export interface IErrorResultFunction {
  (subTitle: string, extra?: React.ReactElement): React.ReactElement
}

export interface IWithErrorPages {
  serverError: IErrorResultFunction;
  unauthorizedError: IErrorResultFunction;
  notFoundError: IErrorResultFunction;
  forbiddenError: IErrorResultFunction;
}

const defaultExtra = (
  <AntButton type="primary" href={RouteObject.homepage}>
    Go Back Home
  </AntButton>
);

function WithErrorPages(WrappedComponent: any): React.ComponentClass {
  return class NewComponent extends React.Component<{},{}> implements IWithErrorPages {

    serverError: IErrorResultFunction = (subTitle, extra?) => {
      return <Result
        status={500}
        title={500}
        subTitle={subTitle}
        extra={extra || defaultExtra}
      />
    };

    unauthorizedError: IErrorResultFunction = (subTitle, extra?) => {
      return <Result
        status={403}
        title={401}
        subTitle={subTitle}
        extra={extra || defaultExtra}
      />
    };

    notFoundError: IErrorResultFunction = (subTitle, extra?) => {
      return <Result
        status={404}
        title={404}
        subTitle={subTitle}
        extra={extra || defaultExtra}
      />
    };

    forbiddenError: IErrorResultFunction = (subTitle, extra?) => {
      return <Result
        status={403}
        title={403}
        subTitle={subTitle}
        extra={extra || defaultExtra}
      />
    };

    render() {
      return <WrappedComponent
        serverError={this.serverError}
        unauthorizedError={this.unauthorizedError}
        notFoundError={this.notFoundError}
        forbiddenError={this.forbiddenError}
        {...this.props }
      />
    }
  }
}

export default WithErrorPages;