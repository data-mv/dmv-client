import React from 'react';
import {Form} from "antd";
import {StyledPassword} from "../input/input.component";
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import {Rule} from "antd/es/form";

interface IProps {
  passwordRules?: Rule[];
  confirmPasswordRules?: Rule[];
}

const PasswordForm: React.FunctionComponent<IProps> = (props) => {
  const passwordRules: Rule[] = [
    {
      required: true,
      message: "Please input your password!",
    }
  ];
  const confirmPasswordRules: Rule[] = [
    {
      required: true,
      message: "Please confirm your password!"
    },
    ({ getFieldValue }) => ({
      validator(_, value) {
        if (!value || getFieldValue('password') === value) {
          return Promise.resolve();
        }
        return Promise.reject('The two passwords that you entered do not match!');
      }
    })
  ];
  return (
    <>
      <div className="isoInputWrapper">
        <Form.Item
          rules={props.passwordRules || passwordRules}
          name="password"
          hasFeedback
        >
          <StyledPassword
            type="password"
            size="small"
            placeholder="New Password"
            iconRender={(visible: boolean) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
          />
        </Form.Item>
      </div>
      <div className="isoInputWrapper">
        <Form.Item
          rules={props.confirmPasswordRules || confirmPasswordRules}
          name="confirm"
          hasFeedback
          dependencies={['password']}
        >
          <StyledPassword
            type="password"
            placeholder="Confirm Password"
            iconRender={(visible: boolean) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
          />
        </Form.Item>
      </div>
    </>
  );
};

export default PasswordForm;