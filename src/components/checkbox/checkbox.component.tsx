import { Checkbox } from 'antd';
import { AntCheckbox } from './checkbox.styles';

const checkbox = AntCheckbox(Checkbox);

export default checkbox;