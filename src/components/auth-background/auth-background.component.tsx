import React from 'react';
import { Link } from 'react-router-dom';
import IntlMessages from '../utility/intlMessages';
import AuthBackgroundStyleWrapper from './auth-background.styles';
import RouteObject from "../../routes";

interface IProps {
  children: React.ReactElement,
  bgImage: string
}

const AuthBackground: React.FC<IProps> = (props) => (
  <AuthBackgroundStyleWrapper bgImage={props.bgImage}>
    <div className="isoContentWrapper">
      <div className="isoContent">
        <div className="isoLogoWrapper">
          <Link to={RouteObject.dashboard}>
            <IntlMessages id="page.title" />
          </Link>
        </div>
        {props.children}
      </div>
    </div>
  </AuthBackgroundStyleWrapper>
);

export default AuthBackground;