import styled from 'styled-components';
// @ts-ignore
import { palette } from 'styled-theme';
import WithDirection from '../../settings/withDirection';

interface IAuthBackgroundStyleProps {
  "data-rtl": string,
  bgImage: string
}

const AuthBackgroundStyleWrapper = styled.div<IAuthBackgroundStyleProps>`
  width: 100%;
  min-height: 100vh;
  height: 100vh;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  position: relative;
  background: url(${props => props.bgImage}) no-repeat center center;
  background-size: cover;

  &:before {
    content: '';
    width: 100%;
    height: 100%;
    display: flex;
    background-color: rgba(0, 0, 0, 0.1);
    position: absolute;
    z-index: 1;
    top: 0;
    left: ${(props)  => (props['data-rtl'] === 'rtl' ? 'inherit' : '0')};
    right: ${(props) => (props['data-rtl'] === 'rtl' ? '0' : 'inherit')};
  }

  .isoContentWrapper {
    width: 500px;
    height: 100%;
    overflow-y: auto;
    z-index: 10;
    position: relative;
  }

  .isoContent {
    min-height: 100%;
    display: flex;
    flex-direction: column;
    padding: 70px 50px;
    position: relative;
    background-color: #ffffff;

    @media only screen and (max-width: 767px) {
      width: 100%;
      padding: 70px 20px;
    }

    .isoLogoWrapper {
      width: 100%;
      display: flex;
      margin-bottom: 45px;
      justify-content: center;
      flex-shrink: 0;
  
      a {
        font-size: 24px;
        font-weight: 300;
        line-height: 1;
        text-transform: uppercase;
        color: ${palette('secondary', 2)};
      }
    }
  }
`;

export default WithDirection(AuthBackgroundStyleWrapper);
