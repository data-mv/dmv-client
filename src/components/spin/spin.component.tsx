import React from "react";
import {Spin} from "antd";
import ContainerSpinStyle from "./spin.styles";

const ContainerSpin: React.FunctionComponent<{size?: "small" | "large" | "default", tip?: string}> = ({size, tip}) => (
  <ContainerSpinStyle>
    <div className="container-spin">
      <Spin tip={tip || "Loading..."} size={size || "large"} />
    </div>
  </ContainerSpinStyle>
);

export default ContainerSpin;
