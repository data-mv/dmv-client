import React from 'react';
import {injectIntl, FormattedMessage } from 'react-intl';

interface IInjectMessage <T extends {}>{
  (props: T): React.ReactElement
}

const InjectMessage: IInjectMessage<{}> = (props) => <FormattedMessage {...props} />;
export default injectIntl<string, any>(InjectMessage, {forwardRef: false});
