import React from 'react';
import { notification } from "antd";
import styled from 'styled-components';
import {AnyAction} from "redux";

interface IConfig {
  description: string | Array<string>,
  message: string,
  onClose?: () => AnyAction | Function
}

const NotificationContent = styled.div`
  display: inline-block;
  font-size: 12px;
  color: rgba(121, 116, 116, 0.85);
`;

const errorNotification = (config: IConfig) => {
  if (config.message) {
    return notification.error({
      duration: 3,
      ...config,
      description: (
        <NotificationContent>
          {
            Array.isArray(config.description) ?
              config.description[0] :
              config.description
          }
        </NotificationContent>
      )
    });
  }
};

const successNotification = (config: IConfig) => {
  if (config.message) {
    return notification.success({
      duration: 5,
      ...config,
      description: (<NotificationContent> {config.description} </NotificationContent>)
    });
  }
};

export {
  errorNotification,
  successNotification
}