import { Input } from 'antd';
import { InputWrapper, PasswordWrapper } from './input.styles';
import WithDirection from '../../settings/withDirection';

const { Password } = Input;

const WDStyledInput = InputWrapper(Input);
const StyledInput = WithDirection(WDStyledInput);

const WDStyledPassword = PasswordWrapper(Password);
const StyledPassword = WithDirection(WDStyledPassword);


export default StyledInput;
export { StyledPassword };