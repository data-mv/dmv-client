import styled from 'styled-components';
// @ts-ignore
import { palette } from 'styled-theme';
import {
  transition,
  borderRadius
} from '../../settings/style-util';
import {Input} from "antd";

const InputWrapper = (ComponentName: typeof Input) => styled(ComponentName)<{"data-rtl": string}>`
  &.ant-input {
    padding: 4px 10px;
    width: 100%;
    height: 35px;
    cursor: text;
    text-align: ${props => (props['data-rtl'] === 'rtl' ? 'right' : 'left')};
    font-size: 13px;
    line-height: 1.5;
    color: ${palette('text', 1)};
    background-color: #fff;
    background-image: none;
    ${borderRadius('4px')};
    ${transition()};

    &.ant-input-lg {
      height: 42px;
      padding: 6px 10px;
    }

    &.ant-input-sm {
      padding: 1px 10px;
      height: 30px;
    }

    &::-webkit-input-placeholder {
      text-align: ${props => (props['data-rtl'] === 'rtl' ? 'right' : 'left')};
      color: ${palette('grayscale', 0)};
    }

    &:-moz-placeholder {
      text-align: ${props => (props['data-rtl'] === 'rtl' ? 'right' : 'left')};
      color: ${palette('grayscale', 0)};
    }

    &::-moz-placeholder {
      text-align: ${props => (props['data-rtl'] === 'rtl' ? 'right' : 'left')};
      color: ${palette('grayscale', 0)};
    }
    &:-ms-input-placeholder {
      text-align: ${props => (props['data-rtl'] === 'rtl' ? 'right' : 'left')};
      color: ${palette('grayscale', 0)};
    }
  }
`;

const PasswordWrapper = (ComponentName: typeof Input.Password) => styled(ComponentName)<{"data-rtl": string}>`
  padding: 6px 10px;
  width: 100%;
  height: 42px;
  cursor: text;
  text-align: ${props => (props['data-rtl'] === 'rtl' ? 'right' : 'left')};
  font-size: 13px;
  line-height: 1.5;
  color: ${palette('text', 1)};
  background-color: #fff;
  background-image: none;
  border: 1px solid ${palette('border', 0)};
  ${borderRadius('4px')};
  ${transition()};
`;

export { InputWrapper, PasswordWrapper };