import React from 'react';
import Nprogress from 'nprogress';
import ReactPlaceholder from "react-placeholder";
import 'nprogress/nprogress.css';
import 'react-placeholder/lib/reactPlaceholder.css';

interface IState {
  component: React.ReactElement | null
}

export default function asyncComponent(importComponent: () => any): React.ComponentClass {
  class AsyncFunc extends React.Component<{}, IState> {
    private mounted: boolean | undefined;

    constructor(props: {}) {
      super(props);
      this.state = {
        component: null
      };
      Nprogress.start();
    }

    public componentWillUnmount() {
      this.mounted = false;
    }

    public async componentDidMount() {
      this.mounted = true;
      const {default: Component} = await importComponent();
      Nprogress.done();
      if (this.mounted) {
        this.setState({
          component: <Component {...this.props} />
        });
      }
    }

    public render() {
      const Component: React.ReactElement = this.state.component || <div />
      return (
        <ReactPlaceholder type='text' rows={7} ready={Component !== null}>
          {Component}
        </ReactPlaceholder>
      );
    }
  }
  return AsyncFunc;
}