import authReducer from "./auth/auth.reducer";
import {IAuthReducer} from "./auth/auth.interface";

export interface Reducer {
  auth:  IAuthReducer
}

const reducer: Reducer = {
  auth: authReducer
};

export default reducer;
