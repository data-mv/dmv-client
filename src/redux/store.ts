import {
  createStore,
  combineReducers,
  applyMiddleware,
  compose,
  Middleware,
  Store
} from 'redux';
import {
  createBrowserHistory,
  BrowserHistory
} from "history";
import {
  routerReducer,
  routerMiddleware
} from 'react-router-redux';
import thunk  from 'redux-thunk';
import logger from 'redux-logger';
import reducers from './reducers';

const history: BrowserHistory = createBrowserHistory();
const routeMiddleware: Middleware = routerMiddleware(history);
const middleware = [thunk, routeMiddleware];
// const composeEnhancers = compose;
// @ts-ignore
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

if (process.env.NODE_ENV === "development") {
  middleware.push(logger);
}

const store: Store = createStore(
  combineReducers({
    ...reducers,
    router: routerReducer
  }),
  composeEnhancers(applyMiddleware(...middleware))
);

export { store, history };
