interface DefaultErrorMessage {
  (message?: string, description?: string): {
    status: "failed",
    status_code: 500,
    message: string,
    formatted_error: string
  }
}

export const defaultErrorMessage: DefaultErrorMessage = (
  message="Operation Failed",
  description= "Sorry, something went wrong"
) => ({
  status: "failed",
  status_code: 500,
  message,
  formatted_error: description
});
