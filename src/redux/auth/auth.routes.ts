const authRoutes = {
  signUpUrl: 'users/',
  loginUrl: 'token/',
  passwordResetUrl: (email: string): string => `password-reset?email=${email}`,
  passwordUpdateUrl: 'password-update/',
  verifyApiKeyUrl: 'verify-apikey/',
};

export default authRoutes;