import makeApiCall, { requestMethods } from "../api";
import {
  authStart, authLoginSuccess, authSignUpSuccess,
  authFailure, passwordResetSuccess, verifyApiKeySuccess,
  passwordUpdateSuccess, verifyApiKeyFailure, activateAccountFailure,
  activateAccountSuccess
} from "./auth.actions";
import {
  ISuccessPayload, IErrorPayload, ISignUpOperation,
  ILoginOperation, IPasswordResetOperation, IPasswordUpdateOperation,
  IVerifyApiKeyOperation
} from "./auth.interface";
import { defaultErrorMessage } from "../utils"
import { Dispatch } from "redux";
import authRoutes from "./auth.routes";
import RouteObject from "../../routes";


export const signUpOperation: ISignUpOperation = (payload) => {
  return async (dispatch: Dispatch) => {
    try {
      dispatch(authStart());
      const response: Response = await makeApiCall(authRoutes.signUpUrl, {method: requestMethods.POST, body: payload});
      if (!response.ok) {
        const error: IErrorPayload = await response.json();
        dispatch(authFailure(error));
      } else {
        const data: ISuccessPayload = await response.json();
        dispatch(authSignUpSuccess(data))
      }
    } catch (err) {
      const error: IErrorPayload = defaultErrorMessage("Registration Failed");
      dispatch(authFailure(error));
    }
  }
};

export const loginOperation: ILoginOperation = (payload) => {
  return async (dispatch: Dispatch) => {
    try {
      dispatch(authStart());
      const response: Response = await makeApiCall(authRoutes.loginUrl, {method: requestMethods.POST, body: payload});
      if (!response.ok) {
        const error: IErrorPayload = await response.json();
        dispatch(authFailure(error));
      } else {
        const data: ISuccessPayload = await response.json();
        dispatch(authLoginSuccess(data));
      }
    } catch (err) {
      const error: IErrorPayload = defaultErrorMessage("Authentication Failed");
      dispatch(authFailure(error));
    }
  }
};

export const passwordResetOperation: IPasswordResetOperation = (payload) => {
  return async (dispatch: Dispatch) => {
    try {
      const { email } = payload;
      const response: Response = await makeApiCall(authRoutes.passwordResetUrl(email));
      if (!response.ok) {
        const error: IErrorPayload = await response.json();
        dispatch(authFailure(error));
      } else {
        const data: ISuccessPayload = await response.json();
        dispatch(passwordResetSuccess(data));
      }
    } catch (err) {
      const error: IErrorPayload = defaultErrorMessage("Password Reset Failed");
      dispatch(authFailure(error));
    }
  }
};

export const passwordUpdateOperation: IPasswordUpdateOperation = (payload) => {
  return async (dispatch: Dispatch) => {
    const { apiKey } = payload;
    try {
      const response: Response = await makeApiCall(
        authRoutes.passwordUpdateUrl,
        {
          method: requestMethods.PUT,
          body: payload,
          headerParams: {'Authorization': `Api-Key ${apiKey}`}
        }
      );
      if (!response.ok) {
        const error: IErrorPayload = await response.json();
        dispatch(authFailure(error))
      } else {
        const data: ISuccessPayload = await response.json();
        dispatch(passwordUpdateSuccess(data));
      }
    } catch (err) {
      const error: IErrorPayload = defaultErrorMessage("Password Update Failed");
      dispatch(authFailure(error));
    }
  }
};

export const verifyApiKeyOperation: IVerifyApiKeyOperation = (payload) => {
  return async (dispatch: Dispatch) => {
    const { apiKey, path } = payload;
    const updatePasswordFlag = (path === RouteObject.updatePassword);
    try {
      const response: Response = await makeApiCall(
        authRoutes.verifyApiKeyUrl,
        {headerParams: { 'Authorization': `Api-Key ${apiKey}`}}
      );
      if (!response.ok) {
        const error = await response.json();
        const action =  updatePasswordFlag ? verifyApiKeyFailure(error) : activateAccountFailure(error);
        dispatch(action);
      } else {
        const data = await response.json();
        const action = updatePasswordFlag ? verifyApiKeySuccess(data) : activateAccountSuccess(data);
        dispatch(action);
      }
    } catch (err) {
      const error = defaultErrorMessage();
      const action =  updatePasswordFlag ? verifyApiKeyFailure(error) : activateAccountFailure(error);
      dispatch(action);
    }
  }
};
