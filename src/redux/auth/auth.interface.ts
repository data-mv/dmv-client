import {Dispatch} from "redux";

export interface IResponse {
  status: string;
  status_code: number;
  message: string;
}

export interface ISuccessPayload extends IResponse {
  data: {
    email?: string,
    access_token?: string,
    refresh_token?: string
  }
}

export interface IErrorPayload extends IResponse {
  error?: any,
  formatted_error: any
}

export interface IAuthStartAction {
  type: string
}

export interface IAuthSuccessAction extends IAuthStartAction {
  payload: ISuccessPayload
}

export interface IAuthFailureAction extends IAuthStartAction {
  payload: IErrorPayload
}

export type IPasswordResetAction = IAuthSuccessAction;

export type IVerifyApiKeyAction = IAuthSuccessAction;

export type IVerifyApiKeyFailureAction = IAuthFailureAction;

export type IPasswordUpdateAction = IAuthSuccessAction;

export type IActivateAccountSuccessAction = IAuthSuccessAction;

export type IActivateAccountFailureAction = IAuthFailureAction;

export interface IAuthState {
  isStarting: true | false;
  isLoggedIn: true | false;
  success: true | false;
  message: string;
  description: string;
  data: ISuccessPayload | null;
  error: IErrorPayload | null;
  verified: boolean;
  activated: boolean;
}

export interface IPayload {
  email: string
}

export interface ISignUpOperation {
  (payload: IPayload & {termsAndConditions?: boolean}): (dispatch: Dispatch) => void
}

export interface ILoginOperation {
  (payload: IPayload & {password: string}): (dispatch: Dispatch) => void
}

export interface IPasswordResetOperation  {
  (payload: IPayload): (dispatch: Dispatch) => void
}

export interface IPasswordUpdateOperation {
  (payload: {password: string, apiKey: string}): (dispatch: Dispatch) => void
}

export interface IVerifyApiKeyOperation {
  (payload: {apiKey: string, path: string}): (dispatch: Dispatch) => void
}

export type IAction =
  | IAuthStartAction
  | IAuthSuccessAction
  | IAuthFailureAction
  | IPasswordResetAction;

export interface IAuthReducer {
  (state: IAuthState, action: IAction): IAuthState
}

export type ISelectPasswordResetState = Pick<IAuthState, "success" | "data" | "error">;

export interface IAuthActionTypes {
  AUTH_START: string;
  AUTH_LOGIN_SUCCESS: string;
  AUTH_SIGN_UP_SUCCESS: string;
  AUTH_FAILURE: string;
  RESET_AUTH_STATE: string;
  PASSWORD_RESET_SUCCESS: string;
  VERIFY_API_KEY_SUCCESS: string;
  PASSWORD_UPDATE_SUCCESS: string;
  VERIFY_API_KEY_FAILURE: string;
  ACTIVATE_ACCOUNT_SUCCESS: string;
  ACTIVATE_ACCOUNT_FAILURE: string;
}
