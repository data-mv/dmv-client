import AuthActionTypes from "./auth.types";
import {
  IAuthStartAction,
  IAuthFailureAction,
  IAuthSuccessAction,
  ISuccessPayload,
  IErrorPayload,
  IPasswordResetAction,
  IVerifyApiKeyAction,
  IPasswordUpdateAction,
  IVerifyApiKeyFailureAction,
  IActivateAccountSuccessAction, IActivateAccountFailureAction
} from "./auth.interface";

export const authStart = (): IAuthStartAction => ({
  type: AuthActionTypes.AUTH_START
});

export const authLoginSuccess = (payload: ISuccessPayload): IAuthSuccessAction => ({
  type: AuthActionTypes.AUTH_LOGIN_SUCCESS,
  payload
});
export const authSignUpSuccess = (payload: ISuccessPayload): IAuthSuccessAction => ({
  type: AuthActionTypes.AUTH_SIGN_UP_SUCCESS,
  payload
});

export const authFailure = (payload: IErrorPayload): IAuthFailureAction => ({
  type: AuthActionTypes.AUTH_FAILURE,
  payload
});

export const resetAuthState = (): IAuthStartAction => ({
    type: AuthActionTypes.RESET_AUTH_STATE
});

export const passwordResetSuccess = (payload: ISuccessPayload): IPasswordResetAction => ({
  type: AuthActionTypes.PASSWORD_RESET_SUCCESS,
  payload
});

export const verifyApiKeySuccess = (payload: ISuccessPayload): IVerifyApiKeyAction => ({
  type: AuthActionTypes.VERIFY_API_KEY_SUCCESS,
  payload
});

export const verifyApiKeyFailure = (payload: IErrorPayload): IVerifyApiKeyFailureAction => ({
  type: AuthActionTypes.VERIFY_API_KEY_FAILURE,
  payload
});

export const passwordUpdateSuccess = (payload: ISuccessPayload): IPasswordUpdateAction => ({
  type: AuthActionTypes.PASSWORD_UPDATE_SUCCESS,
  payload
});

export const activateAccountSuccess = (payload: ISuccessPayload): IActivateAccountSuccessAction => ({
  type: AuthActionTypes.ACTIVATE_ACCOUNT_SUCCESS,
  payload
});

export const activateAccountFailure = (payload: IErrorPayload): IActivateAccountFailureAction => ({
  type: AuthActionTypes.ACTIVATE_ACCOUNT_FAILURE,
  payload
});
