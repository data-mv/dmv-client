import AuthActionTypes from "./auth.types";
import {
  IAuthState, IAuthSuccessAction, IAuthFailureAction,
  IPasswordResetAction, IAuthReducer, IVerifyApiKeyAction,
  IPasswordUpdateAction, IVerifyApiKeyFailureAction, IActivateAccountSuccessAction, IActivateAccountFailureAction
} from "./auth.interface";


const INITIAL_STATE: IAuthState = {
  isStarting: false,
  isLoggedIn: false,
  success: false,
  message: "",
  description: "",
  data: null,
  error: null,
  verified: false,
  activated: false
};

const authReducer: IAuthReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case AuthActionTypes.AUTH_START:
      return {
        ...state,
        isStarting: true,
        error: null
      };
    case AuthActionTypes.AUTH_LOGIN_SUCCESS:
      return {
        ...state,
        isStarting: false,
        isLoggedIn: true,
        success: true,
        data: (action as IAuthSuccessAction).payload,
        error: null
      };
    case AuthActionTypes.AUTH_SIGN_UP_SUCCESS:
      return {
        ...state,
        isStarting: false,
        success: true,
        data: (action as IAuthSuccessAction).payload,
        message: "Account Created",
        description: "Please check your email for your validation link!",
        error: null
      };
    case AuthActionTypes.AUTH_FAILURE:
      return {
        ...state,
        isStarting: false,
        success: false,
        data: null,
        error: (action as IAuthFailureAction).payload
      };
    case AuthActionTypes.RESET_AUTH_STATE:
      return {
        ...state,
        isStarting: false,
        success: false,
        message: "",
        description: "",
        data: null,
        error: null
      };
    case AuthActionTypes.PASSWORD_RESET_SUCCESS:
      return {
        ...state,
        success: true,
        data: (action as IPasswordResetAction).payload
      };
    case AuthActionTypes.PASSWORD_UPDATE_SUCCESS:
      return {
        ...state,
        success: true,
        data: (action as IPasswordUpdateAction).payload,
        message: "Password Updated",
        description: "Please login with your new password"
      };
    case AuthActionTypes.VERIFY_API_KEY_SUCCESS:
      return {
        ...state,
        verified: true,
        data: (action as IVerifyApiKeyAction).payload
      };
    case AuthActionTypes.VERIFY_API_KEY_FAILURE:
      return {
        ...state,
        verified: false,
        error: (action as IVerifyApiKeyFailureAction).payload
      };
    case AuthActionTypes.ACTIVATE_ACCOUNT_SUCCESS:
      return {
        ...state,
        activated: true,
        data: (action as IActivateAccountSuccessAction).payload
      };
    case AuthActionTypes.ACTIVATE_ACCOUNT_FAILURE:
      return {
        ...state,
        activated: false,
        error: (action as IActivateAccountFailureAction).payload
      };
    default:
      return state;
  }
};

export default authReducer;
