import {createSelector, OutputSelector, Selector} from 'reselect';
import {IAuthState, ISelectPasswordResetState} from "./auth.interface";
import {IState} from "../../interface";


export const selectAuth: Selector<IState, IAuthState> = (state: IState): IAuthState => state.auth;

export const selectAuthStart: OutputSelector<IState, boolean, (auth: IAuthState) => boolean> = createSelector(
  [selectAuth],
  (auth) => auth.isStarting
);

export const selectIsLoggedIn: OutputSelector<IState, boolean, (auth: IAuthState) => boolean> = createSelector(
  [selectAuth],
  (auth) => auth.isLoggedIn
);

export const selectPasswordResetState: OutputSelector<IState, ISelectPasswordResetState, (auth: IAuthState) => ISelectPasswordResetState> = createSelector(
  [selectAuth],
  (auth) => ({
    success: auth.success,
    data: auth.data,
    error: auth.error
  })
);

export const selectUpdatePasswordState = createSelector(
  [selectAuth, selectPasswordResetState],
  (authState, passwordResetState) => ({
    verified: authState.verified,
    ...passwordResetState
  })
);

export const selectActivatedAccountState = createSelector(
  [selectAuth],
  (authState) => ({
    success: authState.success,
    error: authState.error,
    activated: authState.activated
  })
);

