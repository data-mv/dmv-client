interface IMakeApiCallParams {
  method?: string,
  body?: {},
  headerParams?: {} | null
}
interface IMakeApiCall {
  (url: string, params?: IMakeApiCallParams): Promise<Response>
}

const baseUrl: string | undefined = process.env.REACT_APP_BASE_URL;

export const requestMethods = {
  GET: 'GET',
  POST: 'POST',
  DELETE: 'DELETE',
  PUT: 'PUT'
};

const defaultHeaderParams: HeadersInit = {
  "Content-Type": "application/json",
  "Accept": "application/json",
};


const makeApiCall: IMakeApiCall = async (url, {method=requestMethods.GET, body={}, headerParams}={}) => {
  const apiUrl = baseUrl + url;
  const headers: Headers = new Headers({...defaultHeaderParams, ...headerParams});

  let requestInit: RequestInit = {
    headers: headers,
    method: requestMethods.GET,
    mode: "cors"
  };


  if (method === requestMethods.DELETE) {
    requestInit = {
      ...requestInit,
      method: method
    }
  }

  if (method === requestMethods.POST || method === requestMethods.PUT) {
    requestInit = {
      ...requestInit,
      method: method,
      body: JSON.stringify(body)
    }
  }

  const request: Request = new Request(apiUrl, requestInit);
  return await fetch(request)
};

export default makeApiCall;

