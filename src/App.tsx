import React from 'react';
import { Provider } from 'react-redux';
import { store } from './redux/store';
import PublicRoutes from './router';
import { ThemeProvider } from 'styled-components';
import { IntlProvider } from 'react-intl';
import themes from './settings/themes';
import { themeConfig } from './settings';
import AppLocale from './languageProvider';
import config, { getCurrentLanguage } from './containers/LanguageSwitcher/config';
import AppStyled from './App.styles';

const currentAppLocale = AppLocale[getCurrentLanguage(config.defaultLanguage || 'english').locale];

const DashApp: React.VoidFunctionComponent<{}> = () => (
  <IntlProvider
    locale={currentAppLocale.locale}
    messages={currentAppLocale.messages}
  >
    <ThemeProvider theme={themes[themeConfig.theme]}>
      <AppStyled>
        <Provider store={store}>
          <PublicRoutes />
        </Provider>
      </AppStyled>
    </ThemeProvider>
  </IntlProvider>
  
);

export default DashApp;
