import React from 'react';

const  rtl: string | null = document.getElementsByTagName('html')[0].getAttribute('dir');
const withDirection = (Component: any) => (props: any) => {
  return <Component {...props} data-rtl={rtl} />;
};

export default withDirection;
export { rtl }