import themedefault, {Itheme} from './themedefault';
import theme2 from './theme2';

interface IThemes {
  [index: string]: Itheme,
  themedefault: Itheme,
  theme2: Itheme
}
const themes: IThemes = {
  themedefault,
  theme2
};

export default themes;
