interface IThemeConfig {
  topbar: string,
  sidebar: string,
  layout: string,
  theme: string
}

const language: string = 'english';

const themeConfig: IThemeConfig = {
  topbar: 'themedefault',
  sidebar: 'themedefault',
  layout: 'themedefault',
  theme: 'themedefault'
};

export {
  language,
  themeConfig
}