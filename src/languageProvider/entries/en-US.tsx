import antdEn from 'antd/lib/locale-provider/en_US';
import enMessages from '../locales/en_US.json';

export interface IEnLang {
  [index: string]: any,
  messages: {[index: string]: string},
  antd: typeof antdEn,
  locale: string
}

const EnLang: IEnLang = {
  messages: {
    ...enMessages,
  },
  antd: antdEn,
  locale: 'en-US'
};
export default EnLang;
