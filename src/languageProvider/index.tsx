import Enlang, {IEnLang} from './entries/en-US';
// import { addLocaleData } from 'react-intl';

interface IAppLocale {
  [index: string]: any,
  en: IEnLang
}
const AppLocale: IAppLocale = { en: Enlang };
// addLocaleData(AppLocale.en.data);

export default AppLocale;
