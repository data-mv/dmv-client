import { language } from '../../settings';

interface ILanguage {
  languageId: string,
  locale: string,
  text: string
}

interface IConfig {
  defaultLanguage: string,
  options: Array<ILanguage>
}

const config: IConfig = {
  defaultLanguage: language,
  options: [
    {
      languageId: 'english',
      locale: 'en',
      text: 'English'
    }
  ]
};

export function getCurrentLanguage(lang: string): ILanguage {
  let selectedLanguage: ILanguage = config.options[0];
  config.options.forEach(language => {
    if (language.languageId === lang) {
      selectedLanguage = language;
    }
  });
  return selectedLanguage;
}

export default config;
