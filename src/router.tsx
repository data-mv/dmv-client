import React from 'react';
import { Route, Redirect, BrowserRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { RouteProps } from 'react-router';
import asyncComponent from './helpers/AsyncFunc';
import {createStructuredSelector, Selector} from "reselect";
import {IState} from "./interface";
import {selectIsLoggedIn} from "./redux/auth/auth.selectors";
import RouteObject from "./routes";
import {Button} from "antd";


type IMapStateToProps = Selector<IState, {
  isLoggedIn: boolean
}>;

type IPublicRoutesProps = ReturnType<IMapStateToProps>;

interface IRestrictedRouteProps extends IPublicRoutesProps, RouteProps {
  component: React.ComponentType<any>
}


const RestrictedRoute: React.FunctionComponent<IRestrictedRouteProps> = ({
  component: Component,
  isLoggedIn,
  ...rest
}) => (
  <Route
    {...rest}
    render={props =>
      isLoggedIn ? (
        <Component {...props} />
      ) : (
        <Redirect
          push
          to={{
            pathname: RouteObject.login,
            state: {from: props.location}
          }}
        />
      )
    }
  />
);


const PublicRoutes: React.FunctionComponent<IPublicRoutesProps> = ({ isLoggedIn }) => {
  return (
    <BrowserRouter >
      <div>
        <RestrictedRoute
          exact
          isLoggedIn={isLoggedIn}
          path={[RouteObject.dashboard, RouteObject.homepage]}
          component={Button}
        />
        <Route
          exact
          path={RouteObject.signUp}
          component={asyncComponent(() => import ('./pages/auth/sign-up/sign-up.component'))}
        />
        <Route
          exact
          path={RouteObject.login}
          component={asyncComponent(() => import('./pages/auth/sign-in/sign-in.component'))}
        />
        <Route
          exact
          path={RouteObject.passwordReset}
          component={asyncComponent(() => import('./pages/auth/password-reset/password-reset.component'))}
        />
        <Route
          exact
          path={RouteObject.updatePassword}
          component={asyncComponent(() => import('./pages/auth/update-password/update-password.component'))}
        />
        <Route
          exact
          path={RouteObject.validateAccount}
          component={asyncComponent(() => import('./pages/auth/account-validation.component'))}
        />
      </div>
    </BrowserRouter>
  )
};

const mapStateToProps: IMapStateToProps = createStructuredSelector({
  isLoggedIn: selectIsLoggedIn
});

export default connect(mapStateToProps)(PublicRoutes);