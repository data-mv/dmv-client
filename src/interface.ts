import {IAuthState} from "./redux/auth/auth.interface";

export interface IState {
  auth: IAuthState
}